angular.module('app.services', ['ionic'])

.factory('User', function($q, $http) {

    var signup = function(data) {
        return $q(function (resolve, reject) {
            $http.post('https://powerful-bastion-3271.herokuapp.com/user', data)
            .success(function(res) {                
                console.log(res);
                resolve(res);
            })
            .error(function(err) {
                console.log(err);
                reject(err);
            });
        })
    }

    var signin = function(data) {
        return $q(function (resolve, reject) {
            $http.post('https://powerful-bastion-3271.herokuapp.com/user/signin', data)
            .success(function(res) {
                console.log(res);
                resolve(res);
            })
            .error(function(err) {
                console.log(err);
                reject(err);
            });
        })
    }

    return {
        signup: signup,
        signin: signin
    }
})

.factory('Post', function($q, $http) {

    var post = function(data) {
        return $q(function (resolve, reject) {
            $http.post('https://powerful-bastion-3271.herokuapp.com/status', data)
            .success(function(res) {                
                console.log(res);
                resolve(res);
            })
            .error(function(err) {
                console.log(err);
                reject(err);
            });
        })
    }

    var get = function(data) {
        return $q(function (resolve, reject) {
            $http.get('https://powerful-bastion-3271.herokuapp.com/status/all')
            .success(function(res) {                
                console.log(res);
                resolve(res);
            })
            .error(function(err) {
                console.log(err);
                reject(err);
            });
        })
    }

    return {
        post: post,
        get: get,
    }
});