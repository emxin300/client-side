angular.module('app.controller', ['app.services'])

.controller('HomeCtrl', function($scope, $http, Post) {
    $scope.posts = [];    

    $scope.refresh = function() {
        Post.get().then(function (res) {
        	$scope.posts = res;
            $scope.data = {};
        }, function(err) {
        	var alertPopup = $ionicPopup.alert({
        		title: 'Load homepage failed!',
        		template: 'Can not load homepage!'
        	});
        });
    };

    $scope.refresh();
})

.controller('SignupCtrl', function($scope, $http, $state, User, $ionicPopup) {
    $scope.data = {};

    $scope.post = function() {
        User.signup($scope.data).then(function (res) {
        	$state.go('page9');
            $scope.data = {};
        }, function(err) {
        	var alertPopup = $ionicPopup.alert({
        		title: 'Signup failed!',
        		template: 'Can not sign up!'
        	});
        });
    };

})

.controller('SigninCtrl', function($state, $scope, $http, User, $ionicPopup) {
    $scope.data = {};

    $scope.post = function() {
        User.signup($scope.data).then(function (res) {
        	$state.go('page1');
            $scope.data = {};
        }, function(err) {
        	var alertPopup = $ionicPopup.alert({
        		title: 'Signin failed!',
        		template: 'Can not signin!'
        	});
        });
    };

})


.controller('VietPostCtrl', function($scope, $http, $state, Post) {
    $scope.data = {};
    $scope.data.user = {user_id: 1, username: 'Tung'};

    $scope.post = function() {       

        Post.post($scope.data).then(function (res) {
        	$state.go('page1');
            $scope.data = {};
        }, function(err) {
        	var alertPopup = $ionicPopup.alert({
        		title: 'Post failed!',
        		template: 'Can not write new post!'
        	});
        });
    };

});