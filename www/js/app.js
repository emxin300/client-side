// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('app', ['ionic', 'app.controller', 'app.services'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
    $ionicConfigProvider.tabs.position("bottom");

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

    .state('page1', {
      url: '/page1',
      cache: false,
      templateUrl: 'page1.html',
      controller: 'HomeCtrl'
    })

    .state('page2', {
      url: '/page2',
      templateUrl: 'page2.html'
    })

    .state('page3', {
      url: '/page3',
      templateUrl: 'page3.html'
    })

    .state('page4', {
      url: '/page4',
      templateUrl: 'page4.html'
    })

    .state('page5', {
      url: '/page5',
      templateUrl: 'page5.html',
      controller: 'VietPostCtrl'
    })

    .state('page6', {
      url: '/page6',
      templateUrl: 'page6.html'
    })

    .state('page7', {
      url: '/page7',
      templateUrl: 'page7.html'
    })

    .state('page8', {
        url: '/page8',
        templateUrl: 'page8.html',
        controller: 'SignupCtrl'
    })

    .state('page9', {
        url: '/page9',
        templateUrl: 'page9.html',
        controller: 'SigninCtrl'
    })

    ;

  // if none of the above states are matched, use this as the fallback

  $urlRouterProvider.otherwise('/page9');


});